# Readme Spam or Ham

## Paper

To build the paper:

```shell
docker-compose up paper
```

## Code

To build the application:

```shell
dotnet build code/
```

To test the application:

```shell
dotnet test code/
```

To run the application:

```shell
dotnet run --project code/WebApp
```
