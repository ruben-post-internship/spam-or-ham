---
title: "Onderzoeken: Security"
subtitle: "*Spam* or *Ham*"
documentclass: scrreprt
author: "Ruben Pera"
date: \today
link-citations: true
header-includes:
  - \usepackage[dutch]{babel}
  - \usepackage{url}
  - \usepackage{siunitx}
  - \usepackage{amsmath}
  - \usepackage{mathtools}
  - \usepackage{siunitx}
  - \usepackage{eurosym}
  - \DeclareSIUnit{\EUR}{\text{\texteuro}}
  - \usepackage{appendix}
  - \usepackage[acronym, toc]{glossaries}
  - \pagestyle{headings}
  - \makenoidxglossaries
  - \newacronym{tdd}{TDD}{Test-driven development}
  - \newacronym{sdlc}{SDLC}{Systems development life cycle}
  - \newacronym{ann}{ANN}{Artificial neural network}
  - \newacronym{ml}{ML}{Machine Learning}
  - \newacronym{relu}{ReLU}{Rectified Linear Unit}
  - \newacronym{sgd}{SGD}{Stochastic gradient descent}
  - \newacronym{rmsprop}{RMSProp}{Root Mean Square Propagation}
  - \newacronym{adam}{Adam}{Adaptive Moment Estimation}
  - \newacronym{mse}{MSE}{Mean Squared Error}
  - \newacronym{hsts}{HSTS}{HTTP Strict Transport Security}
  - \newglossaryentry{plantuml}{name=PlantUML, description={'PlantUML is used to draw UML diagrams, using a simple and human readable text description' citaat van plantuml.com}}
---

<!-- markdownlint-disable MD025 -->

\tableofcontents
