# Inleiding

## Onderwerp, probleem en relevantie {.unnumbered}

Werknemers van InHolland ontvingen de laatste tijd meerdere Engelse berichten van onbekende nummers. Sommige van deze berichten zijn _spam_ en sommige berichten zijn geen _spam_ (_ham_).

Om InHolland te helpen te bepalen welke van de Engelse berichten _spam_ en _ham_ zijn, en om hiermee de veiligheid van de school te vergroten doormiddel van het tegenhouden van mogelijke phishing aanvallen, heeft de school gevraagd om een website te ontwikkelen waarop gecontroleerd kan worden of een bericht _spam_ is of niet.

Tiago A. Almeida and José María Gómez Hidalgo hebben een uitgebreide lijst van sms berichten opgesteld waarbij bij ieder bericht is aangegeven of dit bericht _spam_ of _ham_ (geen _spam_) is. Deze lijst 'SMS Spam Collection v. 1' [@spam-collection] heeft InHolland aangedragen om als initiële data te gebruiken om te bepalen of berichten _spam_ of geen _spam_ zijn.

Daarnaast is de vraag of gebruikers ook zelf nieuwe _spam_ berichten toe kunnen voegen zodat de website over tijd steeds beter wordt in het herkennen van _spam_ berichten en kan leren van nieuwe _spam_ berichten.
