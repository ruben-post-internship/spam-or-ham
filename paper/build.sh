#!/bin/bash

pandoc \
        --bibliography=./paper/bib/paper.bib \
        --number-sections \
        --filter pandoc-citeproc \
        --csl ./paper/csl/ieee.csl \
        -s \
        ./paper/md/paper.md \
        ./paper/md/1.inleiding/*.md \
        ./paper/md/2.probleemstelling/*.md \
        ./paper/md/3.methode/*.md \
        ./paper/md/4.resultaat/*.md \
        ./paper/md/5.conclusie/*.md \
        ./paper/md/6.aanbeveling/*.md \
        ./paper/md/appendix/*.md \
        -o ./paper/paper.pdf