using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using WebApp.database;
using Microsoft.EntityFrameworkCore;
using WebApp.services;
using Microsoft.ML;
using WebApp.models;
using Microsoft.Extensions.ML;
using reCAPTCHA.AspNetCore;

namespace WebApp
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddRazorPages();

            services.AddScoped<ISpamFileService, SpamFileService>();
            services.AddScoped<IMlContextService, MlContextService>();
            services.AddScoped<IMessageService, MessageService>();

            services.AddSingleton<MLContext>(new MLContext());

            services.AddDbContext<IDatabaseContext, DatabaseContext>(options => options.UseSqlite(Configuration["DatabaseContext"]));
            services.AddPredictionEnginePool<Message, MessagePrediction>().FromFile(Configuration["ModelPath"], true);

            services.AddRecaptcha(Configuration.GetSection("RecaptchaSettings"));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHsts();
            app.UseHttpsRedirection();

            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapRazorPages();
            });
        }
    }
}
