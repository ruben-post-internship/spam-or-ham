using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WebApp.entities;

namespace WebApp.database
{
    public interface IDatabaseContext
    {
        DbSet<MessageEntity> MessageEntities { get; }
        Task<int> SaveChangesAsync();
    }

    public class DatabaseContext : DbContext, IDatabaseContext
    {
        public DatabaseContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<MessageEntity> MessageEntities { get; set; }

        public Task<int> SaveChangesAsync()
        {
            return base.SaveChangesAsync();
        }
    }
}