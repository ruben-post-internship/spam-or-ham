﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.RazorPages;
using reCAPTCHA.AspNetCore;
using WebApp.models;
using WebApp.services;

namespace WebApp.Pages
{
    public class IndexModel : PageModel
    {
        private readonly IMessageService _messageService;
        private readonly IRecaptchaService _recaptchaService;

        public Message Message { get; set; } = new Message();
        public string Status { get; set; }

        [BindProperty]
        public string Text { get; set; }

        public IndexModel(IMessageService messageService, IRecaptchaService recaptchaService)
        {
            _recaptchaService = recaptchaService;
            _messageService = messageService;
        }

        public async Task OnPageHandlerSelectionAsync(HttpContext httpContext)
        {
            if (httpContext.Request.Method == "POST")
            {
                var response = await _recaptchaService.Validate(httpContext.Request);

                if (!response.success) throw new System.Exception("There was an error validating the google recaptcha response. Please try again, or contact the site owner.");
            }
        }

        public override async Task OnPageHandlerSelectionAsync(PageHandlerSelectedContext context)
        {
            await OnPageHandlerSelectionAsync(context.HttpContext);
        }

        public override void OnPageHandlerExecuted(PageHandlerExecutedContext _)
        {
            Text = string.Empty;

            ModelState.Clear();
        }

        public void OnPostIsSpam()
        {
            Status = "- Checked text";

            Message = new Message
            {
                IsSpam = _messageService.IsSpam(Text),
                Text = Text
            };
        }

        public async Task OnPostAddSpam()
        {
            Status = "- Added spam message";

            await _messageService.AddSpam(Text);

            Message = new Message
            {
                IsSpam = true,
                Text = Text
            };
        }
    }
}
