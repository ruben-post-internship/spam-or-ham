using WebApp.types;

namespace WebApp.entities
{
    public class MessageEntity
    {
        public int Id { get; set; }
        public bool IsSpam { get; set; }
        public string Text { get; set; }
        public SourceType Source { get; set; }
    }
}