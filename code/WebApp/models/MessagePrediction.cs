using Microsoft.ML.Data;

namespace WebApp.models
{
    public class MessagePrediction
    {
        [ColumnName("PredictedLabel")]
        public bool IsSpam { get; set; }
    }
}