using Microsoft.ML.Data;
using WebApp.entities;
using WebApp.types;

namespace WebApp.models
{
    public class Message
    {
        [ColumnName("Label")]
        public bool IsSpam { get; set; }
        public string Text { get; set; }

        public MessageEntity ToMessageEntity(SourceType source = SourceType.File) => new MessageEntity
        {
            IsSpam = IsSpam,
            Text = Text,
            Source = source
        };

        public static Message FromMessageEntity(MessageEntity messageEntity) => new Message
        {
            IsSpam = messageEntity.IsSpam,
            Text = messageEntity.Text
        };

        public static Message Normalized(string text, bool isSpam = false) => new Message
        {
            IsSpam = isSpam,
            Text = text.ToLower()
        };
    }
}