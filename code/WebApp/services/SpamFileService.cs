using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using WebApp.models;

namespace WebApp.services
{
    public interface ISpamFileService
    {
        Task<IEnumerable<Message>> GetMessagesFromFile();
    }

    public class SpamFileService : ISpamFileService
    {
        private string FilePath = Path.Combine(Directory.GetCurrentDirectory(), "assets", "SMSSpamCollection.txt");

        public async Task<IEnumerable<Message>> GetMessagesFromFile()
        {
            var lines = await GetLines();

            return ReadLines(lines);
        }

        public virtual Task<string[]> GetLines()
        {
            return File.ReadAllLinesAsync(FilePath);
        }

        public virtual IEnumerable<Message> ReadLines(string[] lines)
        {
            return lines
                .Select(x => x.ToLower().Split('\t'))
                .Select(x => Message.Normalized(x.Skip(1).First(), x.First() == "spam"));
        }
    }
}