using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.ML;
using Microsoft.ML.Data;
using Microsoft.ML.Trainers;
using WebApp.database;
using WebApp.models;
using WebApp.types;
using static Microsoft.ML.DataOperationsCatalog;

namespace WebApp.services
{
    public interface IMlContextService
    {
        Task Initialize();
    }
    public class MlContextService : IMlContextService
    {
        private readonly IDatabaseContext _databaseContext;
        private readonly ISpamFileService _spamFileService;
        private readonly MLContext _mLContext;
        private readonly IConfiguration _configuration;

        private SdcaLogisticRegressionBinaryTrainer Trainer => _mLContext.BinaryClassification.Trainers.SdcaLogisticRegression(labelColumnName: "Label", featureColumnName: "Features");

        public MlContextService(IDatabaseContext databaseContext, ISpamFileService spamFileService, MLContext mLContext, IConfiguration configuration)
        {
            _databaseContext = databaseContext;
            _spamFileService = spamFileService;
            _mLContext = mLContext;
            _configuration = configuration;
        }

        public async Task Initialize()
        {
            await InitializeForEvaluation();
        }

        public async Task<(ITransformer model, TrainTestData data)> InitializeForEvaluation()
        {
            await FillDatabase();

            var messages = await GetMessages();

            var data = LoadData(messages);

            var model = Train(data.TrainSet);

            SaveModel(model, data.TrainSet.Schema);

            return (model, data);
        }

        public virtual async Task FillDatabase()
        {
            if (_databaseContext.MessageEntities.Any())
            {
                var messagesFromFile = _databaseContext.MessageEntities.Where(x => x.Source == SourceType.File);

                _databaseContext.MessageEntities.RemoveRange(messagesFromFile);
            }

            var messages = await _spamFileService.GetMessagesFromFile();
            var entities = messages.Select(x => x.ToMessageEntity(SourceType.File));

            _databaseContext.MessageEntities.AddRange(entities);

            await _databaseContext.SaveChangesAsync();
        }

        public virtual ITransformer Train(IDataView trainSet)
        {
            var estimator = _mLContext
                .Transforms
                .Text
                .FeaturizeText(outputColumnName: "Features", inputColumnName: nameof(Message.Text))
                .Append(Trainer);

            return estimator.Fit(trainSet);
        }

        public virtual BinaryClassificationMetrics Evaluate(ITransformer model, IDataView testSet)
        {
            var predictions = model.Transform(testSet);

            return _mLContext.BinaryClassification.Evaluate(predictions, "Label");
        }

        public virtual TrainTestData LoadData(List<Message> messages)
        {
            var dataView = _mLContext.Data.LoadFromEnumerable(messages);
            return _mLContext.Data.TrainTestSplit(dataView, testFraction: 0.2);
        }

        public virtual Task<List<Message>> GetMessages() =>
            _databaseContext.MessageEntities.Select(x =>
                new Message
                {
                    IsSpam = x.IsSpam,
                    Text = x.Text
                }).ToListAsync();

        public virtual void SaveModel(ITransformer model, DataViewSchema schema)
        {
            _mLContext.Model.Save(model, schema, _configuration["ModelPath"]);
        }
    }
}