using System.Threading.Tasks;
using Microsoft.Extensions.ML;
using WebApp.database;
using WebApp.models;
using WebApp.types;

namespace WebApp.services
{
    public interface IMessageService
    {
        bool IsSpam(string text);
        Task AddSpam(string text);
    }
    public class MessageService : IMessageService
    {
        private readonly PredictionEnginePool<Message, MessagePrediction> _predictionEnginePool;
        private readonly IDatabaseContext _databaseContext;

        public MessageService(PredictionEnginePool<Message, MessagePrediction> predictionEnginePool, IDatabaseContext databaseContext)
        {
            _predictionEnginePool = predictionEnginePool;
            _databaseContext = databaseContext;
        }

        public async Task AddSpam(string text)
        {
            var entity = Message.Normalized(text, true).ToMessageEntity(SourceType.User);

            _databaseContext.MessageEntities.Add(entity);

            await _databaseContext.SaveChangesAsync();
        }

        public bool IsSpam(string text)
        {
            return _predictionEnginePool.Predict(Message.Normalized(text)).IsSpam;
        }
    }
}