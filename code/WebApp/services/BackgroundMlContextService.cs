using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace WebApp.services
{
    public class BackgroundMlContextService : BackgroundService
    {
        private readonly IServiceProvider _serviceProvider;
        public BackgroundMlContextService(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public async Task ExecuteAsync(int secondsToDelay, IMlContextService mlContextService, CancellationToken cancellationToken)
        {
            await mlContextService.Initialize();

            await Task.Delay(1000 * secondsToDelay, cancellationToken);
        }

        protected override async Task ExecuteAsync(CancellationToken cancellationToken)
        {
            const int secondsToDelay = 3600; // One hour

            using (var scope = _serviceProvider.CreateScope())
            {
                var mlContextService = scope.ServiceProvider.GetRequiredService<IMlContextService>();

                await ExecuteAsync(secondsToDelay, mlContextService, cancellationToken);
            }
        }
    }
}