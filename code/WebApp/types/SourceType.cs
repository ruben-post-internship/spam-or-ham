namespace WebApp.types
{
    public enum SourceType : byte
    {
        File = 0,
        User = 1,
    }
}