using System.Threading.Tasks;
using WebApp.services;
using Xunit;
using Moq;
using WebApp.Pages;
using reCAPTCHA.AspNetCore;
using Microsoft.AspNetCore.Http;
using System;

namespace WebApp.Tests.pages
{
    public class IndexModelTests
    {
        [Theory]
        [InlineData(true)]
        [InlineData(false)]
        public void Can_OnPostIsSpam(bool isSpam)
        {
            // Arrange
            var messageServiceMock = new Mock<IMessageService>();

            messageServiceMock
                .Setup(x => x.IsSpam(It.IsAny<string>()))
                .Returns(isSpam);

            var model = new IndexModel(messageServiceMock.Object, null);
            model.Text = "Test123";

            // Act
            model.OnPostIsSpam();

            // Assert
            Assert.Equal("- Checked text", model.Status);
            Assert.Equal(isSpam, model.Message.IsSpam);
            Assert.Equal(model.Text, model.Message.Text);

            messageServiceMock.Verify(x => x.IsSpam("Test123"));
        }

        [Fact]
        public async Task Can_OnPostAddSpam()
        {
            // Arrange
            var messageServiceMock = new Mock<IMessageService>();

            var model = new IndexModel(messageServiceMock.Object, null);
            model.Text = "Test123";

            // Act
            await model.OnPostAddSpam();

            // Assert
            Assert.Equal("- Added spam message", model.Status);
            Assert.True(model.Message.IsSpam);
            Assert.Equal(model.Text, model.Message.Text);

            messageServiceMock.Verify(x => x.AddSpam("Test123"));
        }

        [Fact]
        public void Can_OnPageHandlerExecuted()
        {
            // Arrange
            var model = new IndexModel(null, null);

            model.Text = "Test123";
            model.ModelState.AddModelError("Test", "Test123");

            Assert.NotEmpty(model.ModelState.Keys);

            // Act
            model.OnPageHandlerExecuted(null);

            // Assert
            Assert.Empty(model.Text);
            Assert.Empty(model.ModelState.Keys);
        }

        [Theory]
        [InlineData(true)]
        [InlineData(false)]
        public async Task OnPageHandlerSelectionAsync_Throws_When_Invalid(bool success)
        {
            // Arrange
            var recaptchaServiceMock = new Mock<IRecaptchaService>();
            recaptchaServiceMock
                .Setup(x => x.Validate(It.IsAny<HttpRequest>(), It.IsAny<bool>()))
                .ReturnsAsync(new RecaptchaResponse
                {
                    success = success
                });

            var model = new IndexModel(null, recaptchaServiceMock.Object);

            var context = new DefaultHttpContext();
            context.Request.Method = "POST";

            if (success)
            {
                // Act
                await model.OnPageHandlerSelectionAsync(context);
            }
            else
            {
                // Act + Assert
                await Assert.ThrowsAsync<Exception>(() => model.OnPageHandlerSelectionAsync(context));
            }
        }


        [Fact]
        public async Task OnPageHandlerSelectionAsync_Only_On_Post()
        {
            // Arrange
            var recaptchaServiceMock = new Mock<IRecaptchaService>();
            recaptchaServiceMock
                .Setup(x => x.Validate(It.IsAny<HttpRequest>(), It.IsAny<bool>()))
                .ReturnsAsync(new RecaptchaResponse
                {
                    success = false
                });

            var model = new IndexModel(null, recaptchaServiceMock.Object);

            var context = new DefaultHttpContext();
            context.Request.Method = "GEt";

            // Act
            await model.OnPageHandlerSelectionAsync(context);
        }
    }
}