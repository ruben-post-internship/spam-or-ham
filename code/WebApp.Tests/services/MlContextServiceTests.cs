using System.Threading.Tasks;
using WebApp.services;
using Xunit;
using Moq;
using WebApp.database;
using System.Collections.Generic;
using WebApp.entities;
using System.Linq;
using MockQueryable.Moq;
using WebApp.types;
using WebApp.models;
using Microsoft.ML;
using Microsoft.Extensions.Configuration;

namespace WebApp.Tests.services
{
    public class MlContextServiceTests
    {
        private Mock<IDatabaseContext> databaseContextMock = new Mock<IDatabaseContext>();
        private Mock<ISpamFileService> spamFileServiceMock = new Mock<ISpamFileService>();
        private MLContext mLContext = new MLContext();
        private Mock<IConfiguration> configurationMock = new Mock<IConfiguration>();

        public MlContextServiceTests()
        {
            configurationMock
                .Setup(x => x["ModelPath"])
                .Returns("tmp.zip");
        }

        private Mock<MlContextService> GetMockedService()
        {
            return new Mock<MlContextService>(databaseContextMock.Object, spamFileServiceMock.Object, mLContext, configurationMock.Object) { CallBase = true };
        }

        [Fact]
        public async Task FillDatabase_Only_Removes_Existing_File_MessageEntities()
        {
            // Arrange
            var toRemove = new List<MessageEntity>();

            var dbSet = new List<MessageEntity>
            {
                new MessageEntity { Id = 1, Source = SourceType.File },
                new MessageEntity { Id = 2, Source = SourceType.User }
            }.AsQueryable().BuildMockDbSet();

            databaseContextMock
                .Setup(x => x.MessageEntities)
                .Returns(dbSet.Object);

            databaseContextMock
                .Setup(x => x.MessageEntities.RemoveRange(It.IsAny<IEnumerable<MessageEntity>>()))
                .Callback((IEnumerable<MessageEntity> messageEntities) => toRemove.AddRange(messageEntities));

            // Act
            await GetMockedService().Object.FillDatabase();

            // Assert
            databaseContextMock.Verify(x => x.SaveChangesAsync());

            Assert.NotEmpty(toRemove);

            Assert.All(toRemove, x => Assert.Equal(SourceType.File, x.Source));
        }

        [Fact]
        public async Task FillDatabase_Adds_Messages_From_File()
        {
            // Arrange
            var toAdd = new List<MessageEntity>();

            var messages = new List<Message> {
                new Message { IsSpam = true, Text = "Test1" },
                new Message { IsSpam = false, Text = "Test2" }
            };

            databaseContextMock
                .Setup(x => x.MessageEntities)
                .Returns(new List<MessageEntity>().AsQueryable().BuildMockDbSet().Object);

            spamFileServiceMock
                .Setup(x => x.GetMessagesFromFile())
                .ReturnsAsync(messages);

            // Act
            await GetMockedService().Object.FillDatabase();

            // Assert
            spamFileServiceMock.Verify(x => x.GetMessagesFromFile());
            databaseContextMock.Verify(x => x.MessageEntities.AddRange(It.Is<IEnumerable<MessageEntity>>(x =>
                    x.First().IsSpam && x.First().Text.Equals("Test1") && x.First().Source == SourceType.File
                && !x.Last().IsSpam && x.Last().Text.Equals("Test2") && x.Last().Source == SourceType.File
            )));
        }

        [Fact]
        public void Can_LoadData()
        {
            // Arrange
            var messages = new List<Message>
            {
                new Message { IsSpam = true, Text = "Test1" },
                new Message { IsSpam = false, Text = "Test2" }
            };

            // Act
            var result = GetMockedService().Object.LoadData(messages);

            // Assert
            Assert.NotNull(result.TrainSet);
            Assert.NotNull(result.TestSet);
        }

        [Fact]
        public async Task Can_Initialize()
        {
            // Arrange
            var serviceMock = GetMockedService();

            serviceMock
                .Setup(x => x.GetMessages())
                .ReturnsAsync(new List<Message>
                {
                    new Message { IsSpam = true, Text = "Test1" },
                    new Message { IsSpam = false, Text = "Test2" }
                });

            serviceMock
                .Setup(x => x.FillDatabase())
                .Returns(Task.CompletedTask);

            // Act
            await serviceMock.Object.Initialize();

            // Assert
            serviceMock.Verify(x => x.FillDatabase());
            serviceMock.Verify(x => x.Train(It.IsAny<IDataView>()));
            serviceMock.Verify(x => x.SaveModel(It.IsAny<ITransformer>(), It.IsAny<DataViewSchema>()));
        }

        [Theory]
        [InlineData(0.95)]
        [InlineData(0.98)]
        public async Task Trained_Model_Accuracy(double accuracy)
        {
            // Arrange
            var messages = await new SpamFileService().GetMessagesFromFile();

            var serviceMock = new Mock<MlContextService>(
                null,
                null,
                new MLContext(),
                configurationMock.Object
            )
            {
                CallBase = true
            };

            serviceMock
                .Setup(x => x.FillDatabase())
                .Returns(Task.CompletedTask);

            serviceMock
                .Setup(x => x.GetMessages())
                .ReturnsAsync(messages.ToList());

            var service = serviceMock.Object;

            // Act
            var (model, data) = await service.InitializeForEvaluation();

            var metrics = service.Evaluate(model, data.TestSet);

            // Assert
            Assert.InRange(metrics.Accuracy, accuracy, 1.0);
        }
    }
}