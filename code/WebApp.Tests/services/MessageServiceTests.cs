using System.Threading.Tasks;
using WebApp.services;
using Xunit;
using Moq;
using WebApp.database;
using WebApp.entities;
using WebApp.types;
using System.Collections.Generic;
using System.Linq;
using MockQueryable.Moq;

namespace WebApp.Tests.services
{
    public class MessageServiceTests
    {
        private Mock<IDatabaseContext> databaseContextMock = new Mock<IDatabaseContext>();

        private Mock<MessageService> GetMockedService()
        {
            return new Mock<MessageService>(null, databaseContextMock.Object) { CallBase = true };
        }

        [Fact]
        public async Task Can_AddUserSpamMessage()
        {
            // Arrange
            var dbSet = new List<MessageEntity>
            {
                new MessageEntity { Id = 1, Source = SourceType.File },
                new MessageEntity { Id = 2, Source = SourceType.User }
            }.AsQueryable().BuildMockDbSet();

            databaseContextMock
                .Setup(x => x.MessageEntities)
                .Returns(dbSet.Object);

            // Act
            await GetMockedService().Object.AddSpam("Test123");

            // Assert
            databaseContextMock.Verify(x => x.MessageEntities.Add(It.Is<MessageEntity>(x => x.Text.Equals("Test123".ToLower()) && x.IsSpam && x.Source == SourceType.User)));
            databaseContextMock.Verify(x => x.SaveChangesAsync());
        }
    }
}