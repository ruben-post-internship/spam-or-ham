using System.Linq;
using System.Threading.Tasks;
using WebApp.services;
using Xunit;
using Moq;
using System.Collections.Generic;
using WebApp.models;

namespace WebApp.Tests.services
{
    public class SpamFileServiceTests
    {
        [Fact]
        public async Task Can_GetLines()
        {
            // Arrange
            var service = new SpamFileService();

            // Act
            var lines = await service.GetLines();

            // Assert
            Assert.NotEmpty(lines);
        }

        [Fact]
        public void Can_ReadLines()
        {
            // Arrange
            var lines = new string[]{
                "Ham\tHam message",
                "Spam\tSpam message"
            };

            var service = new SpamFileService();

            // Act
            var messages = service.ReadLines(lines);

            // Assert
            Assert.NotEmpty(messages);

            Assert.False(messages.First().IsSpam);
            Assert.True(messages.Last().IsSpam);

            Assert.Equal("Ham message".ToLower(), messages.First().Text);
            Assert.Equal("Spam message".ToLower(), messages.Last().Text);
        }

        [Fact]
        public async Task Can_GetMessagesFromFile()
        {
            // Arrange
            string[] callBackLines = default;

            var serviceMock = new Mock<SpamFileService>() { CallBase = true };

            serviceMock
                .Setup(x => x.GetLines())
                .ReturnsAsync(new string[] { "Line1", "Line2" });

            serviceMock
                .Setup(x => x.ReadLines(It.IsAny<string[]>()))
                .Callback((string[] lines) => callBackLines = lines)
                .Returns(new List<Message>{
                    new Message {IsSpam = true, Text = "Message1"}
                });

            // Act
            var result = await serviceMock.Object.GetMessagesFromFile();

            // Assert
            Assert.NotEmpty(callBackLines);
            Assert.NotEmpty(result);

            Assert.Equal("Line1", callBackLines.First());
            Assert.Equal("Line2", callBackLines.Last());

            Assert.True(result.First().IsSpam);
            Assert.Equal("Message1", result.First().Text);
        }
    }
}