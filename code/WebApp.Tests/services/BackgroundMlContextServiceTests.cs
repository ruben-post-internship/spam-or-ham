using System.Threading.Tasks;
using WebApp.services;
using Xunit;
using Moq;
using System.Threading;
using System.Diagnostics;

namespace WebApp.Tests.services
{
    public class BackgroundMlContextServiceTests
    {
        [Fact]
        public async Task Can_ExecuteAsync()
        {
            // Arrange
            var mLContextServiceMock = new Mock<IMlContextService>();
            var service = new BackgroundMlContextService(null);

            // Act
            var stopWatch = Stopwatch.StartNew();

            await service.ExecuteAsync(1, mLContextServiceMock.Object, CancellationToken.None);

            stopWatch.Stop();

            // Assert
            Assert.InRange(stopWatch.Elapsed.TotalSeconds, 1, 2);

            mLContextServiceMock.Verify(x => x.Initialize());
        }
    }
}